﻿#include <iostream>
#include <cmath>
using namespace std;
class Vector {
public:
    Vector() { x = 0; y = 0; z = 0; }
    Vector(double _x, double _y, double _z) {
        x = _x; y = _y; z = _z;
    }
    double Length() {
        return sqrt(x*x+y*y+z*z);
    }
    void ShowV() {
        cout << "{ " << x << " , " << y << " , " << z << " }" << endl;
    }
private:
    double x;
    double y;
    double z;
};


int main()
{
    Vector v1;
    cout << "Default values: "; v1.ShowV();
    cout << "Length = " << v1.Length()<<endl;
    Vector v2(1, 2, 3);
    cout << "Our values: "; v2.ShowV();
    cout << "Length = " << v2.Length()<<endl;
    return 0;
}
